import React, { useState } from 'react';

import { v4 as randomId } from 'uuid';
import { useNavigate } from 'react-router-dom';

import Input from '../../common/Input/Input';
import { Button } from '../../common/Button/Button';

import { dateGenerator } from '../../helpers/dateGenerator';
import { pipeDurationTime } from '../../helpers/pipeDuration';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRotateRight } from '@fortawesome/free-solid-svg-icons';

import styles from './CreateCourse.module.scss';

const CreateCourse = ({ authorsList, setAuthorsList, setCoursesList }) => {
	const navigate = useNavigate();

	const [newAuthor, setNewAuthor] = useState('');

	const [authors, setAuthors] = useState(authorsList);
	const [courseAuthors, setCourseAuthors] = useState([]);

	const [course, setCourse] = useState({
		id: randomId(),
		title: '',
		description: '',
		creationDate: dateGenerator(),
		duration: 0,
		authors: [],
	});

	const addNewAuthor = () => {
		const id = randomId();
		setAuthors([...authors, { id: id, name: newAuthor }]);
		setAuthorsList([...authorsList, { id: id, name: newAuthor }]);
	};

	const addAuthor = (id) => {
		setCourseAuthors([
			...courseAuthors,
			authors.find((author) => author.id === id),
		]);
		setAuthors(authors.filter((author) => author.id !== id));
	};
	const deleteAuthor = (id) => {
		setAuthors([...authors, courseAuthors.find((author) => author.id === id)]);
		setCourseAuthors(courseAuthors.filter((author) => author.id !== id));
	};

	const createCourse = () => {
		setCourse({ ...course, authors: courseAuthors.map((author) => author.id) });

		if (
			course.title.length < 3 ||
			course.title.description < 5 ||
			course.title.duration < 1 ||
			course.title.authors < 1
		) {
			alert('fill in all fields');
		} else {
			setCoursesList((prevState) => [
				...prevState,
				{ ...course, authors: courseAuthors.map((author) => author.id) },
			]);
			navigate('/');
		}
	};

	return (
		<div className={'container'}>
			<div className={styles.createCourse}>
				<div className={styles.title}>
					<div className={styles.titleInput}>
						<Input
							placeholderText={'Enter title...'}
							labelText={'Title'}
							onChange={(e) => setCourse({ ...course, title: e.target.value })}
						/>
					</div>
					<Button text={'Create course'} onClick={createCourse} />
				</div>
				<div className={styles.description}>
					<label htmlFor='description'>Description</label>
					<textarea
						value={course.description}
						id='description'
						placeholder={'Enter description...'}
						onChange={(e) =>
							setCourse({ ...course, description: e.target.value })
						}
					/>
					<FontAwesomeIcon
						onClick={() => setCourse({ ...course, description: '' })}
						className={styles.icon}
						icon={faArrowRotateRight}
					/>
				</div>
				<div className={styles.authorForm}>
					<div className={styles.authorFormContainer}>
						<h3>Add author</h3>
						<Input
							placeholderText={'Enter author name...'}
							labelText={'Author name'}
							onChange={(e) => setNewAuthor(e.target.value)}
						/>
						<Button text={'Create author'} onClick={addNewAuthor} />
						<h3>Duration</h3>
						<Input
							type='number'
							placeholderText={'Enter duration in minutes...'}
							labelText={'Duration'}
							onChange={(e) =>
								setCourse({ ...course, duration: e.target.value })
							}
						/>
						<p className={styles.durationHours}>
							Duration: {pipeDurationTime(course.duration)}
						</p>
					</div>
					<div className={styles.authorFormContainer}>
						<h3>Authors</h3>
						{authors.length ? (
							authors.map((author) => (
								<div key={author.id} className={styles.addAuthorContainer}>
									<p>{author.name}</p>
									<Button
										text={'Add author'}
										onClick={() => addAuthor(author.id)}
									/>
								</div>
							))
						) : (
							<p>Author list is empty</p>
						)}
						<h3>Course authors</h3>
						{courseAuthors.length ? (
							courseAuthors.map((author) => (
								<div key={author.id} className={styles.addAuthorContainer}>
									<p>{author.name}</p>
									<Button
										text={'Delete author'}
										onClick={() => deleteAuthor(author.id)}
									/>
								</div>
							))
						) : (
							<p>Author list is empty</p>
						)}
					</div>
				</div>
			</div>
		</div>
	);
};

export default CreateCourse;
