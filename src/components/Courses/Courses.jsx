import React, { useEffect, useState } from 'react';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import { Button } from '../../common/Button/Button';
import { useNavigate } from 'react-router-dom';

import { getAuthors } from '../../helpers/getAuthors';

import styles from './Courses.module.scss';

const Courses = ({ authorsList, coursesList }) => {
	const navigate = useNavigate();
	const handelGoToNewCourse = () => {
		return navigate('/courses/add');
	};

	const [searchQuery, setSearchQuery] = useState('');
	const [sortedCourses, setSortedCourses] = useState(coursesList);

	const getSortedCourses = () => {
		if (searchQuery) {
			setSortedCourses(
				[...coursesList].filter(
					(course) =>
						course.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
						course.id.toLowerCase().includes(searchQuery)
				)
			);
		} else {
			setSortedCourses(coursesList);
		}
	};

	useEffect(() => {
		if (searchQuery.length < 1) {
			setSortedCourses(coursesList);
		}
	}, [searchQuery]);

	return (
		<div className={'container'}>
			<div className={styles.courses}>
				<div className={styles.bar}>
					<SearchBar
						searchQuery={searchQuery}
						setSearchQuery={setSearchQuery}
						getSortedCourses={getSortedCourses}
					/>
					<Button text={'Add new course'} onClick={handelGoToNewCourse} />
				</div>
				{sortedCourses.map((card) => (
					<CourseCard
						data={card}
						authors={getAuthors(card.authors, authorsList)}
						key={card.id}
					/>
				))}
			</div>
		</div>
	);
};

export default Courses;
