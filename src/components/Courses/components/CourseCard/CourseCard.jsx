import React from 'react';
import styles from './CourseCard.module.scss';
import { Button } from '../../../../common/Button/Button';
import {
	pipeDurationDate,
	pipeDurationTime,
} from '../../../../helpers/pipeDuration';
import { useNavigate } from 'react-router-dom';
const CourseCard = ({
	data: { title, description, creationDate, duration, id },
	authors,
}) => {
	const navigate = useNavigate();

	return (
		<div className={styles.card}>
			<div className={styles.contentContainer}>
				<h3>{title}</h3>
				<p>{description}</p>
			</div>
			<div className={styles.infoContainer}>
				<p>
					Authors:<span>{authors}</span>
				</p>
				<p>
					Duration:<span>{pipeDurationTime(duration)}</span>
				</p>
				<p>
					Created:<span>{pipeDurationDate(creationDate)}</span>
				</p>
				<Button
					text={'Show course'}
					onClick={() => navigate(`/courses/${id}`)}
				/>
			</div>
		</div>
	);
};

export default CourseCard;
