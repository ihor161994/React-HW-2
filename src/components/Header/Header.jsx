import React from 'react';

import { Logo } from './components/Logo/Logo';
import { Button } from '../../common/Button/Button';

import styles from './Header.module.scss';
import { useLocation, useNavigate } from 'react-router-dom';

const Header = ({ user, setUser }) => {
	const location = useLocation().pathname;

	const isTools = () => {
		return location === '/login' || location === '/registration';
	};

	const navigate = useNavigate();
	const handleLogout = () => {
		localStorage.removeItem('token');
		setUser({
			email: null,
			name: null,
		});
		navigate('/login');
	};
	const handleLogin = () => {
		navigate('/login');
	};

	return (
		<div className='container'>
			<div className={styles.header}>
				<Logo />
				{!isTools() && (
					<div className={styles.tools}>
						{!user.name ? (
							<Button text={'Login'} onClick={handleLogin} />
						) : (
							<>
								<div className={styles.userName}>{user.name}</div>
								<Button text={'Logout'} onClick={handleLogout} />
							</>
						)}
					</div>
				)}
			</div>
		</div>
	);
};

export default Header;
