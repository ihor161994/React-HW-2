import React from 'react';

import styles from './CourseInfo.module.scss';
import { Link, useParams } from 'react-router-dom';
import { getAuthors } from '../../helpers/getAuthors';

const CourseInfo = ({ authorsList, coursesList }) => {
	const { courseId } = useParams();
	const course = coursesList.filter((course) => course.id === courseId)[0];

	if (!course)
		return (
			<div className={'container'}>
				<div className={styles.courseInfo}>
					<Link to={'/courses'} className={styles.back}>
						&#8592; Back to courses
					</Link>
					<h3 className={styles.title}>Sorry, not found...</h3>
				</div>
			</div>
		);

	const { title, creationDate, description, duration, id, authors } = course;
	return (
		<div className={'container'}>
			<div className={styles.courseInfo}>
				<Link to={'/courses'} className={styles.back}>
					&#8592; Back to courses
				</Link>
				<h3 className={styles.title}>{title}</h3>
				<div className={styles.blocksContainer}>
					<div className={styles.description}>
						<p>{description}</p>
					</div>
					<div className={styles.detailInfo}>
						<p>
							<span>ID: </span>
							{id}
						</p>
						<p>
							<span>Duration: </span>
							{duration}
						</p>
						<p>
							<span>Created: </span>
							{creationDate}
						</p>
						<p>
							<span>Authors: </span>
							{getAuthors(authors, authorsList)}
						</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
