import Header from './components/Header/Header';
import './App.css';
import Courses from './components/Courses/Courses';
import {
	BrowserRouter as Router,
	Navigate,
	Route,
	Routes,
} from 'react-router-dom';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { mockedCoursesList, mockedAuthorsList } from './datas/data';

import { useEffect, useState } from 'react';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';

function App() {
	const [authorsList, setAuthorsList] = useState(mockedAuthorsList);
	const [coursesList, setCoursesList] = useState(mockedCoursesList);
	const [user, setUser] = useState({
		email: null,
		name: null,
	});

	useEffect(() => {
		if (localStorage.getItem('token')) {
			async function getUser() {
				try {
					const response = await fetch('http://localhost:4000/users/me', {
						method: 'GET',
						headers: {
							'Content-Type': 'application/json',
							Authorization: localStorage.getItem('token'),
						},
					});

					const result = await response.json();
					await setUser({
						name: result.result.name,
						email: result.result.email,
					});
				} catch (e) {
					console.log(e.response);
				}
			}

			getUser();
		}
	}, []);

	return (
		<div className='App'>
			<Router>
				<Header user={user} setUser={setUser} />

				<Routes>
					<Route exact path='/registration' element={<Registration />} />
					<Route exact path='/login' element={<Login setUser={setUser} />} />

					<Route exact path='/courses'>
						<Route
							index
							element={
								<Courses authorsList={authorsList} coursesList={coursesList} />
							}
						/>
						<Route
							exact
							path=':courseId'
							element={
								<CourseInfo
									authorsList={authorsList}
									coursesList={coursesList}
								/>
							}
						/>
						<Route
							exact
							path='add'
							element={
								<CreateCourse
									authorsList={authorsList}
									setAuthorsList={setAuthorsList}
									setCoursesList={setCoursesList}
								/>
							}
						/>
					</Route>
					<Route
						path='*'
						element={
							!!user.name ? (
								<Navigate to={'/courses'} />
							) : (
								<Navigate to={'/login'} />
							)
						}
					/>
				</Routes>
			</Router>
		</div>
	);
}

export default App;
